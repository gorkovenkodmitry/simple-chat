# coding: utf-8


import sys
import socket
import select

HOST = '' 
SOCKET_LIST = []
RECV_BUFFER = 4096 
PORT = 9009
user_names = {}


def get_client_name(sock):
    try:
        return user_names[sock.getpeername()[1]]
    except KeyError:
        return u'%s:%s' % sock.getpeername()


def chat_server():

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind((HOST, PORT))
    server_socket.listen(10)
 
    # добавить текущий сокет в список читаемых
    SOCKET_LIST.append(server_socket)
 
    print u'Запущен на порту ' + str(PORT)
 
    while 1:

        ready_to_read,ready_to_write,in_error = select.select(SOCKET_LIST,[],[],0)
      
        for sock in ready_to_read:
            # новый запрос на соединение
            if sock == server_socket: 
                sockfd, addr = server_socket.accept()
                SOCKET_LIST.append(sockfd)
                print u'Клиент (%s, %s) подключен' % addr
                send_msg(sockfd, u'Для изменения имени пользователя пришлите "name - [Ваше Имя]"\n'.encode('utf-8'))
                 
                broadcast(server_socket, sockfd, (u'[%s:%s] вошел в чат комнату\n' % addr).encode('utf-8'))
             
            # сообщение от клиента
            else:
                # пробуем разобрать
                try:
                    # получим из буфера
                    data = sock.recv(RECV_BUFFER)
                    if data:
                        # отправим широковещательный запрос
                        if data[:6] == 'name -':
                            client_name = data[7:].decode('utf-8').replace('\n', '')
                            if client_name not in user_names.values():
                                send_msg(sock, (u'Ваше имя в чате: "%s"\n' % client_name).encode('utf-8'))
                                message = u'[%s] теперь [%s]\n' % (get_client_name(sock), client_name)
                                user_names[sock.getpeername()[1]] = client_name
                                broadcast(server_socket, sock, message.encode('utf-8'))
                            else:
                                send_msg(sock, (u'Имя "%s" уже занято\n' % client_name).encode('utf-8'))
                        else:
                            broadcast(server_socket, sock, "\r" + '[' + get_client_name(sock).encode('utf-8') + '] ' + data)  
                    else:
                        # удалим из списка если пусто
                        if sock in SOCKET_LIST:
                            SOCKET_LIST.remove(sock)

                        # пустые данные, значит уберем из соединения
                        broadcast(server_socket, sock, (u'Клиент [%s] отключился\n' % get_client_name(sock)).encode('utf-8'))

                # ошибка 
                except:
                    broadcast(server_socket, sock, (u'Клиент [%s] отключился\n' % get_client_name(sock)).encode('utf-8'))
                    continue

    server_socket.close()
    
# широковещательный запрос отправим всем
def broadcast (server_socket, sock, message):
    for socket in SOCKET_LIST:
        # отправить конкретному пиру
        if socket != server_socket and socket != sock :
            send_msg(socket, message)


# сообщение конкретному пользователю
def send_msg(sock, message):
    try :
        sock.send(message)
    except :
        # ошибочное соединение
        sock.close()
        # уберем из списка
        if sock in SOCKET_LIST:
            SOCKET_LIST.remove(sock)

 
if __name__ == "__main__":

    sys.exit(chat_server())   