# coding: utf-8
import sys
import socket
import select
 

def chat_client():
    if(len(sys.argv) < 3) :
        print u'Адрес хоста, порт'
        sys.exit()

    host = sys.argv[1]
    port = int(sys.argv[2])
     
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(2)
     
    # пробуем подключиться
    try :
        s.connect((host, port))
    except :
        print u'Не могу подключиться'
        sys.exit()
     
    print u'Подключение к чат серверу, вы можете послать сообщение'
    # sys.stdout.write(u'[Я] '); sys.stdout.flush()
     
    while 1:
        socket_list = [sys.stdin, s]
         
        # список доступных для чтения сокетов
        ready_to_read,ready_to_write,in_error = select.select(socket_list , [], [])
         
        for sock in ready_to_read:             
            if sock == s:
                # входящее сообщение от сервера
                data = sock.recv(4096)
                if not data :
                    print u'\nОтключен от чат сервера'
                    sys.exit()
                else :
                    # вывод на экран
                    sys.stdout.write(data.decode('utf-8'))
                    sys.stdout.write(u'[Я] '); sys.stdout.flush()     
            
            else :
                # вводим сообщение
                msg = sys.stdin.readline()
                s.send(msg)
                sys.stdout.write(u'[Я] '); sys.stdout.flush() 

if __name__ == "__main__":

    sys.exit(chat_client())